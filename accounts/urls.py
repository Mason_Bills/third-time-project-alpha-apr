from django.urls import path
from accounts.views import user_login, user_logout, user_signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
# Had to delete original urls.py after it was named incorrectly as ulrs.py. kept getting "TypeError: 'module' object is not iterable". solution was to delete original urls file and repopulate it with urls
